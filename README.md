# Amazon Mechanical Turk Connector

Amazon Mechanical Turk API Reference

Documentation: https://docs.aws.amazon.com/mturk-requester

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/mturk-requester/2017-01-17/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

